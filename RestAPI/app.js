const express = require('express');
const path = require('path');
const router = express.Router();
var http = require('http');
var nStatic = require('node-static');

const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const cors = require('cors');


const app = express();

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}

app.use(allowCrossDomain);

app.get('/api', (req,res) => {
    res.send('hello');
});

app.use(bodyParser.json());

app.route('/sendEmail').put((req, res) => {

    var transporter = nodemailer.createTransport({
        // example with google mail service
        host: 'smtp.gmail.com',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: 'sara.resume85@gmail.com', // replace by your email to practice
          pass: 'Karthik@85' // replace by your-password
        }
      });
       
      var mailOptions = {
        from:  req.body.firstname + ","+ req.body.email ,
        to: 'sara.resume85@gmail.com ,' + req.body.email,
        subject: 'Message from saraResume',
        text:  req.body.message ,
        
        // html body
        html: '<h1>Dear Sara!</h1><p>This mail has been sent from <b>' + req.body.firstname + '</b>  <br> emailadress: <b>' + req.body.email + '</b> <br>' + req.body.message +'</p>'
      };
       
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) 
        { 
           res.status(400).send({ message: 'Email sending Failed' });
            return console.log(error);
        }
       
        console.log('Email sent: ' + info.response);
         res.status(250).send({ message: 'Email sent' });
      });

      //res.send({status:201, message: 'Some Connection Error. Try Later.' });

});


app.use(express.static('public')); 

// 'public' is my build folder, you can add yours. this will take care of all static files.
app.use('/*', express.static('public/index.html'));

var fileServer = new nStatic.Server('public');
app.get('/*', (req, res) => {
  console.log (req.path);
  fileServer.serve(req, res);
});

app.listen(process.env.port || 8080,()=> console.log ('server started'));