import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators  }  from '@angular/forms';
import { FeedbackCls, contactType } from '../shared/feedback';
import { flyInOut,expand } from '../animations/app.animation';
import { FeedbackService } from '../services/feedback.service';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
    animations: [
      flyInOut(),expand()
    ]
})
export class ContactComponent implements OnInit {

  FeedbackFormGrp: FormGroup;
  feedbackCls: FeedbackCls;
  conType = contactType;
  msg: string;
  feedback: FeedbackCls;
  errMess:string;
  saving:boolean=false;
  formDisplay:boolean=true;
  successDisplay:boolean=false;

  @ViewChild('fform') feedbackFormDirective;


  constructor( private fb: FormBuilder, 
    private feedBackService: FeedbackService) {   
    this.createForm();
  }

  ngOnInit() {
  }

  formErrors = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': ''
  };

  validationMessages = {
    'firstname': {
      'required':      'First Name is required.',
      'minlength':     'First Name must be at least 2 characters long.',
      'maxlength':     'FirstName cannot be more than 25 characters long.'
    },
    'lastname': {
      'required':      'Last Name is required.',
      'minlength':     'Last Name must be at least 2 characters long.',
      'maxlength':     'Last Name cannot be more than 25 characters long.'
    },
    'telnum': {
      'required':      'Tel. number is required.',
      'pattern':       'Tel. number must contain only numbers.'
    },
    'email': {
      'required':      'Email is required.',
      'email':         'Email not in valid format.'
    },
  };

  createForm(): void{
    this.FeedbackFormGrp = this.fb.group ({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      lastname:['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      telnum:[0, [Validators.required , Validators.pattern]],
      email:['', [Validators.required, Validators.email]],
      agree:false,
      contacttype:'None',
      message:''
    });

    this.FeedbackFormGrp.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {

    if(!this.FeedbackFormGrp) {return}

    const form = this.FeedbackFormGrp;


    for (const field in this.formErrors)
    {


      if(this.formErrors.hasOwnProperty(field))
      {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);



        if(control && control.dirty && !control.valid)
        {
          const messages = this.validationMessages[field]; 

          for(const key in control.errors)
          {

            if(control.errors.hasOwnProperty(key))
            {
              this.formErrors[field] += messages[key] + ' ';
            }
          }

        }      
      }
    }
      
    }

onSubmit(){
  this.feedbackCls = this.FeedbackFormGrp.value;
  
  //Spinner displayed : on Submit
  this.saving=true;

  if(this.FeedbackFormGrp.valid)
  {
    //Form Hide
    this.formDisplay = false;
    

    this.feedBackService.postFeedback(this.feedbackCls).subscribe(feedbackCls => {this.feedback = feedbackCls; this.saving=false; this.successDisplay= true;}, 
      errMess =>  {this.errMess= <any>errMess;this.saving=false;this.formDisplay = true;this.successDisplay= false;} );

      //Success Form Displayed for 5seconds
      setTimeout(()=>{    
        this.formDisplay = true;this.successDisplay=false;
   }, 5000);
  }


  console.log(this.feedbackCls);
    this.FeedbackFormGrp.reset({
      firstname: '',
      lastname: '',
      telnum: 0,
      email: '',
      agree: false,
      contacttype: 'None',
      message: ''
    });

    this.feedbackFormDirective.resetForm();
}


}
