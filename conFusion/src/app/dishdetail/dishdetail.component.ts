import { Component, OnInit, ViewChild,Inject } from '@angular/core';

import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { Comment } from '../shared/comment';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators  }  from '@angular/forms';

import { switchMap } from 'rxjs/operators';
import { isNull } from 'util';
import { visibility,flyInOut,expand  } from '../animations/app.animation';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
    animations: [
      flyInOut(),
      visibility(),
      expand()
    ]

 
})
export class DishdetailComponent implements OnInit {

  dish: Dish;

  dishIds: string[];
  prev: string;
  next: string;
  commentCls: Comment;
  dishcopy: Dish;
  formGrp: FormGroup;
  visibility = 'shown';

  errMess: string;

  commentErr = {
    'author': '',
    'comment': ''   
  };

  @ViewChild('commentform') commentForm;


  constructor( private dishservice: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private formBuilder: FormBuilder,
    @Inject('BaseURL') private baseURL ) { this.createForm();}

  ngOnInit() {

    /*
    const id = this.route.snapshot.params['id'];
    //this.dish = this.dishservice.getDish(id);

    this.dishservice.getDish(id).subscribe(dish => this.dish = dish);
    */

    this.dishservice.getDishIds().subscribe( dishIds => this.dishIds = dishIds,
      errmess => this.errMess = <any>errmess);

     this.route.params.pipe(switchMap((params: Params) => 
    {this. visibility = 'hidden'; return this.dishservice.getDish(+params['id']); }))
    .subscribe(dish => {this.dish = dish;this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
    errmess => this.errMess = <any>errmess);

 
    
  }

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

   goBack(): void {
    this.location.back();
  }

  createForm(): void{
    this.formGrp = this.formBuilder.group ({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      rating: 5,
      comment:['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      date:''
    });

    this.formGrp.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  validationMessages = {
    'author': {
      'required':      'Name is required.',
      'minlength':     'Name must be at least 2 characters long.',
      'maxlength':     'Name cannot be more than 25 characters long.'
    },
    'comment': {
      'required':      'Comment is required.',
      'minlength':     'Comment must be at least 2 characters long.',
      'maxlength':     'Comment cannot be more than 50 characters long.'
    }
  };

  onSubmit(){
    this.commentCls = this.formGrp.value;
    this.commentCls.date = Date.now().toString();
    if(isNull(this.commentCls.rating) || !this.commentCls.rating)
    { this.commentCls.rating = 5;}

    if(this.formGrp.status == "VALID")
    {  
      this.dishcopy.comments.push(this.commentCls);
      this.dishservice.putDish(this.dishcopy).subscribe(dish => {this.dish = dish; this.dishcopy=dish;}, 
        errMess => { this.dishcopy = null; this.dish = null; this.errMess= <any>errMess;} );
      
    } 
    
   

    console.log(this.commentCls);
      this.formGrp.reset({
        author: '',
        rating: 1,
        comment: ''
      });

      this.commentForm.resetForm
        ({
          rating: 5,
          author: '',
          comment: ''
        });
  
  }

  onValueChanged(data?: any) {

    if(!this.formGrp) {return}

    const form = this.formGrp;


    for (const field in this.commentErr)
    {

      if(this.commentErr.hasOwnProperty(field))
      {
        // clear previous error message (if any)
        this.commentErr[field] = '';
        const control = form.get(field);

        if(control && control.dirty && !control.valid)
        {
          const messages = this.validationMessages[field]; 
          for(const key in control.errors)
          {
            if(control.errors.hasOwnProperty(key))
            {
              this.commentErr[field] += messages[key] + ' ';
            }
          }

        }      
      }
    }
      
    }

}
