import { Injectable } from '@angular/core';
import { Promotion } from '../shared/promotion';

import { Observable,of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHttpmsgService } from './process-httpmsg.service';


@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(private http: HttpClient,
    private processHTTPMsgService: ProcessHttpmsgService) { }

  /* Comment Line Starts
  getPromotions(): Promotion[] {
    return PROMOTIONS;
  }

  getPromotion(id: string): Promotion {
    return PROMOTIONS.filter((promo) => (promo.id === id))[0];
  }

  getFeaturedPromotion(): Promotion {
    return PROMOTIONS.filter((promotion) => promotion.featured)[0];
  }
  

  getPromotions(): Promise<Promotion[]> {
    return Promise.resolve(PROMOTIONS);
  }

  getPromotion(id: string): Promise<Promotion> {
    return Promise.resolve(PROMOTIONS.filter((promo) => (promo.id === id))[0]);
  }
   getFeaturedPromotion(): Observable<Promotion> {

    return new Promise(resolve => {
      //Simulate server latency with 2 seconds delay
      setTimeout(() => resolve(PROMOTIONS.filter((promotion) => promotion.featured)[0]),2000);
    });


  }

  
  
  Comment Line Ends */



  getPromotions(): Observable<Promotion[]> {
    return this.http.get<Promotion[]>(baseURL+'promotions').pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getPromotion(id:string): Observable<Promotion> {

    return this.http.get<Promotion>(baseURL+'promotions/' + id).pipe(catchError(this.processHTTPMsgService.handleError));

  }

   getFeaturedPromotion(): Observable<Promotion> {

       return this.http.get<Promotion>(baseURL+'promotions?featured=true').pipe(map(promotion => promotion[0])).pipe(catchError(this.processHTTPMsgService.handleError));
   }
}

