import { Injectable } from '@angular/core';
import { Leader } from '../shared/leader';
import { Leaders } from '../shared/leaders';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHttpmsgService } from './process-httpmsg.service';


@Injectable({
  providedIn: 'root'
})
export class LeaderService {

  constructor(private http: HttpClient,
    private processHTTPMsgService: ProcessHttpmsgService) { }

  /* Comment Line Starts
  
  getLeaders(): Leader[] {
    return Leaders;
  }

  getLeader(id:string): Leader {
    return Leaders.filter((Leader) => (Leader.id === id))[0];
  }

  getFeaturedLeader(): Leader {
    return Leaders.filter((Leader) =>(Leader.featured))[0];
  }

  //Mehod Using Promises
  getLeaders(): Promise<Leader[]> {
  return Promise.resolve(Leaders);
}

getLeader(id:string): Promise<Leader> {
  return Promise.resolve(Leaders.filter((Leader) => (Leader.id === id))[0]);
}

getFeaturedLeader(): Promise<Leader> {
  return Promise.resolve(Leaders.filter((Leader) =>(Leader.featured))[0]);
}


//Method using Observables
getLeaders(): Observable<Leader[]> {
  return of(Leaders).pipe(delay(2000));
}

getLeader(id:string): Observable<Leader> {
  return of(Leaders.filter((Leader) => (Leader.id === id))[0]);
}

getFeaturedLeader(): Observable<Leader> {
  return of(Leaders.filter((Leader) =>(Leader.featured))[0]);
}
 Comment Line Ends
*/

//Method using Observables
getLeaders(): Observable<Leader[]> {
  return this.http.get<Leader[]>(baseURL+'leadership').pipe(catchError(this.processHTTPMsgService.handleError));
}

getLeader(id:string): Observable<Leader> {
  return this.http.get<Leader>(baseURL+'leadership/'+id).pipe(catchError(this.processHTTPMsgService.handleError));
}

getFeaturedLeader(): Observable<Leader> {
  return this.http.get<Leader>(baseURL+'leadership?featured=true').pipe(map(leader => leader[0] )).pipe(catchError(this.processHTTPMsgService.handleError));

}

 }