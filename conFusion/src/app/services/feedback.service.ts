import { Injectable } from '@angular/core';
import { FeedbackCls } from '../shared/feedback';

import { Observable,of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHttpmsgService } from './process-httpmsg.service';
 
@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private http: HttpClient,
    private processHTTPMsgService: ProcessHttpmsgService) { }

    //Function to add the FeedBack
    postFeedback(feedback: FeedbackCls): Observable<FeedbackCls> {

      const httpOptions = {
        headers: new HttpHeaders ( {
          'Content-Type':'application/json'
        })
      };

      return this.http.post<FeedbackCls>(baseURL+'feedback', feedback, httpOptions).pipe(catchError(this.processHTTPMsgService.handleError));
    }
}
