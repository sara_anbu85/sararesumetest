import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DishdetailtestComponent } from './dishdetailtest.component';

describe('DishdetailtestComponent', () => {
  let component: DishdetailtestComponent;
  let fixture: ComponentFixture<DishdetailtestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DishdetailtestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DishdetailtestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
